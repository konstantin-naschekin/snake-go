package models

import (
	"testing"
)

func TestIsEmpty(t *testing.T) {
	cell := Cell{}
	if !cell.IsEmpty() {
		t.Error("new initialized cell must be empty")
	}
	notEmptyCell := Cell{Object: MakeSnake(1)}
	if notEmptyCell.IsEmpty() {
		t.Error("cell with snake must be not empty")
	}
}
