package models

// Cell struct contains information about game object and it's direction
type Cell struct {
	Direction Direction
	Object    Recognizable
}

// MakeCell funciton is the constructor for the Cell struct
func MakeCell(direction Direction, object Recognizable) *Cell {
	return &Cell{Direction: direction, Object: object}
}

// IsEmpty function checks if there is no object into the cell
func (cell *Cell) IsEmpty() bool {
	return cell == nil || cell.Object == nil
}
