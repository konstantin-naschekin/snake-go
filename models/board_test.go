package models

import "testing"

func TestMakeBoard(t *testing.T) {
	sizeX := 4
	sizeY := 6
	board := MakeBoard(sizeX, sizeY)
	if board.SizeX != sizeX {
		t.Errorf("incorrect size X = %v, expected %v", board.SizeX, sizeX)
	}
	if board.SizeY != sizeY {
		t.Errorf("incorrect size Y = %v, expected %v", board.SizeY, sizeY)
	}
	if board.Cells == nil {
		t.Error("cells have not been created")
	}
}

func TestHasPoint(t *testing.T) {
	board := MakeBoard(5, 5)
	point1 := MakePoint(0, 0)
	point2 := MakePoint(-1, 0)
	point3 := MakePoint(0, -1)
	point4 := MakePoint(4, 4)
	point5 := MakePoint(5, 4)
	point6 := MakePoint(4, 5)
	point7 := MakePoint(5, 5)
	point8 := MakePoint(2, 3)
	if board.HasPoint(point2) ||
		board.HasPoint(point3) ||
		board.HasPoint(point5) ||
		board.HasPoint(point6) ||
		board.HasPoint(point7) {
		t.Error("incorrect points on the board")
	}
	if !board.HasPoint(point1) ||
		!board.HasPoint(point4) ||
		!board.HasPoint(point8) {
		t.Error("actual points are not recognized on the board")
	}
}

func TestGetAt(t *testing.T) {
	board := MakeBoard(2, 2)
	_, err := board.GetAt(MakePoint(1, 1))
	if err != nil {
		t.Error("incorrect receiving behavior")
	}
	_, err1 := board.GetAt(MakePoint(-1, -1))
	if err1 == nil {
		t.Error("incorrect point validation")
	}
}

func TestSetAt(t *testing.T) {
	board := MakeBoard(2, 2)
	point := MakePoint(1, 1)
	err := board.SetAt(point, *MakeSnake(1))
	if err == nil {
		t.Error("pointer validation is incorrect")
	}
	snake := MakeSnake(1) // pointer
	err1 := board.SetAt(point, snake)
	if err1 != nil {
		t.Error("must correctly set pointer to the point")
	}
	result, err2 := board.GetAt(point)
	if err2 != nil {
		t.Error("unexpected error on getting snake")
	}
	if snake != result.(*Snake) {
		t.Error("result is not pointer to the created snake")
	}
	err3 := board.SetAt(MakePoint(-1, 5), MakeSnake(1))
	if err3 == nil {
		t.Error("snake has been set to an incorrect point")
	}
}
