package models

import (
	"testing"
)

func TestIsValid(t *testing.T) {
	if !Top.IsValid() {
		t.Error("Top must be valid direction")
	}
	if !Bottom.IsValid() {
		t.Error("Bottom must be valid direction")
	}
	if !Left.IsValid() {
		t.Error("Left must be valid direction")
	}
	if !Right.IsValid() {
		t.Error("Right must be valid direction")
	}
	if Direction("invalid").IsValid() {
		t.Error("Passed invalid direction")
	}
	if Direction("another_invalid_direction").IsValid() {
		t.Error("Passed invalid direction")
	}
}

func TestAtRight(t *testing.T) {
	if Top.AtRight() != Right {
		t.Error("Top at right must be right")
	}
	if Right.AtRight() != Bottom {
		t.Error("Right at right must be bottom")
	}
	if Bottom.AtRight() != Left {
		t.Error("Bottom at right must be left")
	}
	if Left.AtRight() != Top {
		t.Error("Left at right must be top")
	}
	if Direction("INVALID").AtRight().IsValid() {
		t.Error("Invalid direction can't be valid")
	}
}

func TestAtLeft(t *testing.T) {
	if Top.AtLeft() != Left {
		t.Error("Top at left must be left")
	}
	if Right.AtLeft() != Top {
		t.Error("Right at left must be top")
	}
	if Bottom.AtLeft() != Right {
		t.Error("Bottom at left must be right")
	}
	if Left.AtLeft() != Bottom {
		t.Error("Left at left must be bottom")
	}
	if Direction("INVALID").AtLeft().IsValid() {
		t.Error("Invalid direction can't be valid")
	}
}

func TestReverse(t *testing.T) {
	if Top.Reverse() != Bottom {
		t.Error("Opposite side for Top is Bottom")
	}
	if Right.Reverse() != Left {
		t.Error("Opposite side for Right is Left")
	}
	if Bottom.Reverse() != Top {
		t.Error("Opposite side for Bottom is Top")
	}
	if Left.Reverse() != Right {
		t.Error("Opposite side for Left is Right")
	}
	if Direction("INVALID").Reverse().IsValid() {
		t.Error("Invalid direction can't be valid")
	}
}
