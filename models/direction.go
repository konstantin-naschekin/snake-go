package models

// Direction is string to point to the next step
type Direction string

const (
	// Top direction
	Top = Direction("TOP")

	// Bottom direction
	Bottom = Direction("BOTTOM")

	// Left direction
	Left = Direction("LEFT")

	// Right direciton
	Right = Direction("RIGHT")
)

// IsValid is validation function for direction
func (direction Direction) IsValid() bool {
	return direction == Top || direction == Bottom || direction == Left || direction == Right
}

// AtRight function retreives next direction at right from current
func (direction Direction) AtRight() Direction {
	switch direction {
	case Top:
		return Right
	case Right:
		return Bottom
	case Bottom:
		return Left
	case Left:
		return Top
	default:
		return Direction("INVALID")
	}
}

// AtLeft function retreives next direction at left from current
func (direction Direction) AtLeft() Direction {
	switch direction {
	case Top:
		return Left
	case Right:
		return Top
	case Bottom:
		return Right
	case Left:
		return Bottom
	default:
		return Direction("INVALID")
	}
}

// Reverse function returns opposite direction to the current
func (direction Direction) Reverse() Direction {
	switch direction {
	case Top:
		return Bottom
	case Right:
		return Left
	case Bottom:
		return Top
	case Left:
		return Right
	default:
		return Direction("INVALID")
	}
}
