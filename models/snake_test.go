package models

import (
	"testing"
)

func TestGetLength(t *testing.T) {
	snake1 := Snake{}
	if snake1.GetLength() != 1 {
		t.Errorf("length of the snake1 is 1, actual is %v", snake1.GetLength())
	}
	snake2 := Snake{}
	snake2.Prev = &Snake{Next: &snake2}
	if snake2.GetLength() != 2 {
		t.Errorf("length of the snake2 is 2, actual is %v", snake2.GetLength())
	}
	snake3 := Snake{}
	snake3.Prev = &Snake{Next: &snake3}
	snake3.Prev.Prev = &Snake{Next: snake3.Prev}
	if snake3.GetLength() != 3 {
		t.Errorf("length of the snake3 is 3, actual is %v", snake3.GetLength())
	}
}

func TestIsHead(t *testing.T) {
	snake := Snake{}
	snake.Prev = &Snake{Next: &snake}
	snake.Prev.Prev = &Snake{Next: snake.Prev}
	if !snake.IsHead() {
		t.Error("incorrect head detection")
	}
	if snake.Prev.IsHead() {
		t.Error("this part of snake's body is not head")
	}
	if snake.Prev.Prev.IsHead() {
		t.Error("the tail is not the head")
	}
}

func TestIsTail(t *testing.T) {
	snake := Snake{}
	snake.Prev = &Snake{Next: &snake}
	snake.Prev.Prev = &Snake{Next: snake.Prev}
	if snake.IsTail() {
		t.Error("it is the snake head, not tail")
	}
	if snake.Prev.IsTail() {
		t.Error("this part of snake's body is not the tail")
	}
	if !snake.Prev.Prev.IsTail() {
		t.Error("this is is tail, incorrect detection")
	}
}

func TestGettingHeadAndTail(t *testing.T) {
	snake := Snake{}
	snake.Prev = &Snake{Next: &snake}
	snake.Prev.Prev = &Snake{Next: snake.Prev}
	head := snake.Prev.Prev.GetHead()
	tail := snake.GetTail()
	if head != &snake {
		t.Error("got incorrect head")
	}
	if tail != snake.Prev.Prev {
		t.Error("got incorrect tail")
	}
}

func TestMakeSnake(t *testing.T) {
	length := 15
	snake := MakeSnake(length)
	if snake.GetLength() != length {
		t.Errorf("It is not correct length of the snake. Expected: %v, Actual: %v", length, snake.GetLength())
	}
}

func TestSnakeGetType(t *testing.T) {
	snake := MakeSnake(5)
	snakeType := snake.GetType()
	if snakeType != SnakeType {
		t.Errorf("type of snake is incorrect: %v", snakeType)
	}
}
