package models

// Point is the model of the game board point
type Point struct {
	X int
	Y int
}

// MakePoint produces a new Point
func MakePoint(x int, y int) Point {
	return Point{x, y}
}

// EqualsPoints checks if two points refer to the same place
func EqualsPoints(first Point, second Point) bool {
	return first.X == second.X && first.Y == second.Y
}

// EqualsTo is a method of Point struct for comparison
func (point Point) EqualsTo(anotherPoint Point) bool {
	return EqualsPoints(point, anotherPoint)
}

// NextTo is helper to find coordinates of the next point
func (point Point) NextTo(direction Direction) Point {
	var x, y int = point.X, point.Y
	switch direction {
	case Top:
		y--
	case Bottom:
		y++
	case Left:
		x--
	case Right:
		x++
	}
	return MakePoint(x, y)
}
