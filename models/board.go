package models

import (
	"errors"
	"reflect"
)

// Board is the main playground of the game
type Board struct {
	SizeX int
	SizeY int
	Cells map[Point]*Cell
}

// MakeBoard is the constructor function for Board
func MakeBoard(sizeX int, sizeY int) *Board {
	board := Board{
		SizeX: sizeX,
		SizeY: sizeY,
		Cells: make(map[Point]*Cell),
	}

	for i := 0; i < sizeX; i++ {
		for j := 0; j < sizeY; j++ {
			point := MakePoint(i, j)
			board.Cells[point] = MakeCell(Top, nil)
		}
	}

	return &board
}

// HasPoint is a method for validation of passed point
func (board *Board) HasPoint(point Point) bool {
	return point.X < board.SizeX && point.X >= 0 && point.Y < board.SizeY && point.Y >= 0
}

// GetAt is a helping function for getting game object by point
func (board *Board) GetAt(point Point) (element Recognizable, err error) {
	if !board.HasPoint(point) {
		err = errors.New("Invalid point coordinates")
	} else {
		return board.Cells[point].Object, nil
	}

	return
}

// SetAt is a helping function for setting game object at the point
func (board *Board) SetAt(point Point, element Recognizable) (err error) {
	if !board.HasPoint(point) {
		err = errors.New("Invalid point coordinates")
	} else if reflect.ValueOf(element).Kind() != reflect.Ptr {
		err = errors.New("element must be a pointer to a game object")
	} else {
		board.Cells[point].Object = element
	}

	return
}

// GetCellBy is method to find cell by a reference to an object
func (board *Board) GetCellBy(object Recognizable) (cell *Cell) {
	for i := 0; i < board.SizeX; i++ {
		for j := 0; j < board.SizeY; j++ {
			cell := board.Cells[MakePoint(i, j)]
			if cell.Object == object {
				return cell
			}
		}
	}
	return nil
}
