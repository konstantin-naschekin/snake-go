package models

// Snake struct is an element of entire snake
type Snake struct {
	Next *Snake
	Prev *Snake
}

// MakeSnake is the snake constructor
func MakeSnake(length int) *Snake {
	snake := &Snake{}
	for i := 1; i < length; i++ {
		newSnake := Snake{Next: snake}
		snake.Prev = &newSnake
		snake = &newSnake
	}
	return snake.GetHead()
}

// IsHead method checks if current part of the snake's body is the head
func (snake *Snake) IsHead() bool {
	return snake.Next == nil
}

// IsTail method checks if current part of the snake's body is the tail
func (snake *Snake) IsTail() bool {
	return snake.Prev == nil
}

// GetHead receives the head of the snake
func (snake *Snake) GetHead() *Snake {
	part := snake
	for !part.IsHead() {
		part = part.Next
	}
	return part
}

// GetTail receives the tail of the snake
func (snake *Snake) GetTail() *Snake {
	part := snake
	for !part.IsTail() {
		part = part.Prev
	}
	return part
}

// GetLength checks length of the snake
func (snake *Snake) GetLength() int {
	s := snake.GetHead()
	counter := 1
	for !s.IsTail() {
		s = s.Prev
		counter++
	}
	return counter
}

// GetType is method of any game object on the board for getting its type
func (snake Snake) GetType() string {
	return SnakeType
}
