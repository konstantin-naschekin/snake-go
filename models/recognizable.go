package models

const (
	// SnakeType is const representing snake object game type
	SnakeType = "SNAKE"
)

// Recognizable is representation of board object
type Recognizable interface {
	GetType() string
}
