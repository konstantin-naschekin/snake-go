package models

import (
	"testing"
)

func TestEqualsTo(t *testing.T) {
	point1 := MakePoint(3, 24)
	point2 := MakePoint(7, 11)
	point3 := MakePoint(3, 24)

	if point1.EqualsTo(point2) {
		t.Error("Points 1 and 2 are not equal")
	}
	if !point1.EqualsTo(point3) {
		t.Error("Points 1 and 3 are equal")
	}
}

func TestEqualsPoints(t *testing.T) {
	point1 := MakePoint(1, 1)
	point2 := MakePoint(1, 1)
	point3 := MakePoint(2, 2)

	if !EqualsPoints(point1, point2) {
		t.Error("Points 1 and 2 are equal")
	}

	if EqualsPoints(point1, point3) || EqualsPoints(point2, point3) {
		t.Error("Points 1 and 3 are not equals")
		t.Error("Points 2 and 3 are not equals")
	}
}

func TestNextAt(t *testing.T) {
	initial := MakePoint(1, 1)

	atTop := initial.NextTo(Top)
	atBottom := initial.NextTo(Bottom)
	atLeft := initial.NextTo(Left)
	atRight := initial.NextTo(Right)

	if atTop.X != 1 && atTop.Y != 0 {
		t.Error("invalid point coordinates at top")
	}

	if atBottom.X != 1 && atBottom.Y != 2 {
		t.Error("invalid point coordinates at bottom")
	}

	if atLeft.X != 0 && atLeft.Y != 1 {
		t.Error("invalid point coordinates at left")
	}

	if atRight.X != 2 && atRight.Y != 1 {
		t.Error("invalid point coordinates at right")
	}
}
